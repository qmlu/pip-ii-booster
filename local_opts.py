#!/usr/bin/env python

# The local_options file must be named local_opts.py and placed
# in the Synergia2 job manager search path.

from synergia_workflow import options

# Any instance of the Options class will be added as a suboption.
opts = options.Options('local')
#opts.add("ompnumthreads",2,"number of openmp threads")

# Any instance of the Override class will be used to override
#   the defaults of the existing options.
override = options.Override()
override.account = "accelsim"
override.numproc = 16
override.procspernode=16
# The location of the setup.sh for your synergia build
#override.setupsh="/wclustre/accelsim/egstern/syn2-devel-pre3/install/bin/setup.sh"
override.setupsh="/work1/accelsim/egstern/syn2-devel-pre3/install/bin/setup.sh"
override.template="job_example_wilson"
override.resumetemplate="resume_example_wilson"
#override.templatepath="full path to template file"
override.queue="cpu_gce"
override.walltime="24:00:00"
