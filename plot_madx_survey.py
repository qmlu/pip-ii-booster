#!/usr/bin/env python3
import sys,os
import numpy as np
import matplotlib.pyplot as plt

mdx_dir = 'pip-ii-booster-madx-files'


s,l,angle,x,y,z,theta,phi,psi,globaltilt = np.loadtxt(mdx_dir + '/survey_pip2.outx', skiprows=8, usecols=(2,3,4,5,6,7,8,9,10,11),unpack=True)

plt.title('s')
plt.plot(s)

plt.figure()
plt.title('l')
plt.plot(l)

plt.figure()
plt.title('angle')
plt.plot(angle)

plt.figure()
plt.title('x')
plt.plot(x)

plt.figure()
plt.title('y')
plt.plot(y)

plt.figure()
plt.title('z')
plt.plot(z)

plt.figure()
plt.title('theta')
plt.plot(theta)

plt.figure()
plt.title('phi')
plt.plot(phi)

plt.figure()
plt.title('psi')
plt.plot(psi)

plt.figure()
plt.title('globaltilt')
plt.plot(globaltilt)

plt.figure()
plt.title('x-z')
plt.plot(x,z,'.')

print('xmax: ', x.max(), ' xmin: ', x.min())
xmid = (x.max()-x.min())/2
t = np.arctan2(y, (x-xmid))

plt.figure()
plt.title('t')
plt.plot(t)

plt.show()

