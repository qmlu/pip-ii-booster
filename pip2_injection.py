#!/usr/bin/env python3

from __future__ import print_function
import sys, os
import numpy as np
import synergia
import pickle
import painting
import tfs

DEBUG = False

import booster_lattice2
from booster_fixup2 import *
from write_elems_pickle import write_elems_pickle

import tfs

from pip2_injection_options import opts

########################################################################


# manage linac injections into the booster
class linac_injector:

    def __init__(self, linac_covariance, booster_freq, linac_freq, seed, commxx):
        self.bunch_covar = linac_covariance
        self.booster_period = 1.0/booster_freq
        self.linac_period = 1.0/linac_freq
        self.linac_time = -self.booster_period/2.0
        self.means = np.zeros(6, 'd')
        self.limits = np.array([6.0, 6.0, 6.0, 6.0, 2.5, 6.0])
        self.commxx = commxx
        self.dist = synergia.foundation.Random_distribution(seed, self.commxx)

        if commxx.get_rank() == 0:
            print("Booster period: ", self.booster_period)
            print("Linac period: ", self.linac_period)

    # determine bunchlet_times for on-momentum injection
    def on_momentum_bunchlet_times(self):
        # keep bunchlets that fall between [-0.7/2, -0.15/2] and [0.15/2, 0.7/2] of the 
        keep_inj = lambda p: ((p>-0.7/2) and (p<-0.15/2)) or ((p>0.15/2) and (p<0.7/2))
        # how many longitudinal bunchlets can fit in this bunch?
        bunchlet_times = []
        # keep times in units of the bunch period
        while self.linac_time < self.booster_period/2:
            if keep_inj(self.linac_time/self.booster_period):
                bunchlet_times.append(self.linac_time)
            # click to next linac bucket
            self.linac_time += self.linac_period

        # we've gone past the current bucket
        self.linac_time -= self.booster_period

        # now add enough linac period to go to 83 booster periods
        for i in range(83):
            # go to next bucket
            while self.linac_time < self.booster_period/2:
                self.linac_time += self.linac_period
            self.linac_time -= self.booster_period

        if self.commxx.get_rank() == 0:
            print("   linac bunchlets: ", len(bunchlet_times), file=logger)
            print("   linac bunchlet times: ", bunchlet_times)

        return bunchlet_times

    # determine bunchlet_times for on-momentum injection
    def off_momentum_bunchlet_times(self):
        # how many longitudinal bunchlets can fit in this bunch?
        # in units of booster periods [-0.5, 0.5] corresponding to
        # phase [-pi, pi]
        bunchlet_times = []
        # keep times in units of the bunch period -.55/2, .55/2
        keep_inj = lambda p: ((p>-0.55/2) and (p<0.55/2))

        while self.linac_time < self.booster_period/2:
            if keep_inj(self.linac_time/self.booster_period):
                bunchlet_times.append(self.linac_time)
            # click to next linac bucket
            self.linac_time += self.linac_period

        # we've gone past the current bucket
        self.linac_time -= self.booster_period

        # now add enough linac period to go to 83 booster periods
        for i in range(83):
            # go to next bucket
            while self.linac_time < self.booster_period/2:
                self.linac_time += self.linac_period
            self.linac_time -= self.booster_period

        if self.commxx.get_rank() == 0:
            print("   linac bunchlets: ", len(bunchlet_times), file=logger)
            print("   linac bunchlet times: ", bunchlet_times)

        return bunchlet_times

    # generate new bunchlet to inject
    def bunchlet(self, refpart, num_particles, real_charge, logger):

        if opts.on_momentum:
            bunchlet_times = self.on_momentum_bunchlet_times()
        else:
            bunchlet_times = self.off_momentum_bunchlet_times()

        # Now I can generate the correct number of linac pulses
        bunch = synergia.bunch.Bunch(refpart, 0, 0.0, self.commxx)

        for btime in bunchlet_times:
            newbunch = synergia.bunch.Bunch(refpart, num_particles, real_charge, self.commxx)
            
            if opts.linac_beam == "realistic":
                synergia.bunch.populate_6d_truncated(self.dist, newbunch, self.means, self.bunch_covar, self.limits)
            elif opts.linac_beam == "pencil":
                # pencil beam has all particles at center
                local_particles = newbunch.get_local_particles()
                local_particles[:,0:6] = 0.0
            elif opts.linac_beam == "cdr":
                synergia.bunch.populate_6d(self.dist, newbunch, self.means, self.bunch_covar)

            local_particles = newbunch.get_local_particles()
            # bunchlet_times are in tune units while particle coordinates
            # are in units of c*t
            local_particles[:, 4] = synergia.foundation.pconstants.c * btime

            # Injection position at elevation of 113 mm
            local_particles[:, 2] += 0.113

            # offset momentum and x position if injecting off-momentum
            if not opts.on_momentum:
                local_particles[:, 5] += 7.0e-4
                # have to shift the injection point to the outside of the
                # ring which is +x in MadX coordinates
                local_particles[:, 0] += +0.0015

            #print("adding new bunchlet at time: ", btime)
            bunch.inject(newbunch)

        #print("returning bunch, total particles: ", bunch.get_total_num())
        return bunch

########################################################################

nnodes = os.getenv('SLURM_NNODES')
nodeid = os.getenv('SLURM_NODEID')
localid = os.getenv('SLURM_LOCALID')
procid = os.getenv('SLURM_PROCID')
nprocs = os.getenv('SLURM_NPROCS')

print('Proc: {}/{}, running in process: {} on node: {}/{}'.format(procid, nprocs, localid, nodeid, nnodes))

logger = synergia.utils.Logger(0, "logger.out")

# create the lattice with default parameters.  This will be modified
# during the injection process.

#lattice_madx = booster_lattice2.read_booster(ORB_scale=1, HP_scale=0, VP_scale=0)
lattice_madx = booster_lattice2.read_booster(ORB_scale=0, HP_scale=0, VP_scale=0)

reader = synergia.lattice.MadX_reader()
reader.parse(lattice_madx)
orig_lattice = reader.get_lattice('booster_foil')
del reader
lattice = fixup_elements(orig_lattice, dogs_to_drifts=False)

mp = synergia.foundation.pconstants.mp # proton mass

# turn on the apertures if requested

if opts.apertures:
    print('Apertures ON', file=logger)
    # turn on all appropriate apertures
    set_apertures(lattice)

else:
    print('Apertures OFF', file=logger)

if opts.chef_propagate:
    print("Using CHEF to propagate", file=logger)
    use_chef(lattice)
elif opts.libff:
    print("using libff to propagate", file=logger)
    use_libff(lattice)
elif opts.libff_norf:
    print("using libff to propagate except RF cavities", file=logger)
    use_libff_except_rf(lattice)
elif opts.chef_map:
    print("using chef maps to propagate", file=logger)
    use_maps(lattice)
else:
    print("Using default to propagate", file=logger)

print("lattice length: ", lattice.get_length(), file=logger)
print("lattice total angle: ", lattice.get_total_angle(), file=logger)

refpart = lattice.get_reference_particle()
energy = refpart.get_total_energy()
momentum = refpart.get_momentum()
gamma = refpart.get_gamma()
beta = refpart.get_beta()

print("beam energy: ", energy, file=logger)
print("beam momentum: ", momentum, file=logger)
print("gamma: ", gamma, file=logger)
print("beta: ", beta, file=logger)

set_rfcavity_voltage(lattice, opts.rf_voltage)

commxx = synergia.utils.Commxx()
myrank = commxx.get_rank()
mpisize = commxx.get_size()

if myrank == 0:
    f = open("booster_lattice.txt", "w")
    print(lattice.as_string(), file=f)
    f.close()
    synergia.utils.write_lsexpr_file(lattice.as_lsexpr(), "booster.lsx")

# make an independent stepper for calculations of lattice functions and
# RF frequency
dummy_stepper = synergia.simulation.Independent_stepper(lattice, 1, opts.steps)    
lattice_simulator = dummy_stepper.get_lattice_simulator()

chef_beamline = lattice_simulator.get_chef_lattice().get_beamline()
f = open("booster_beamline.txt", 'w')
print(synergia.lattice.chef_beamline_as_string(chef_beamline),file=f)
f.close()

# extract useful parameters
lattice_simulator.register_closed_orbit()
closed_orbit_length = lattice_simulator.get_closed_orbit_length()

# register_closed_orbit() works for CHEF.  For libff, we need to do
dummy_stepper.get_lattice_simulator().tune_circular_lattice(1.0e-6)

# register closed orbit/tune_circular_lattice sets the RF cavity frequencies
rf_frequency = lattice_simulator.get_rf_frequency()

print("closed orbit length: ", closed_orbit_length, file=logger)
print("RF frequency: ", rf_frequency, file=logger)

bucket_length = beta * synergia.foundation.pconstants.c/rf_frequency
print("Bucket length: ", bucket_length, file=logger)

if myrank == 0:
    f = open("booster_lattice_after_tune.txt", "w")
    print(lattice.as_string(), file=f)
    f.close()
    synergia.utils.write_lsexpr_file(lattice.as_lsexpr(), "tuned_booster.lsx")

    # # save element names and types for offline programs
    # elems = []
    # s = 0.0
    # for e in lattice.get_elements():
    #     elems.append((e.get_name(), e_get_type(), s, e.get_length()))
    #     s = s + e.get_length()

    # with f = open('elems.pickle', 'wb') as f:
    #     pickle.save("elems.npy", f)

map = lattice_simulator.get_linear_one_turn_map()
print("one turn map", file=logger)
print(np.array2string(map, max_line_width=200), file=logger)
l, v = np.linalg.eig(map)
print("eigenvalues: ", file=logger)
for z in l:
    print("|z|: ", abs(z), " z: ", z, " tune: ", np.log(z).imag/(2.0*np.pi), file=logger)

lf = lattice_simulator.get_lattice_functions(lattice_simulator.get_lattice().get_elements()[-1])
print(file=logger)
print("booster lattice functions", file=logger)
print("beta_x: ", lf.beta_x, " alpha_x: ", lf.alpha_x, "horizontal tune: ", lf.psi_x/(2*np.pi), file=logger)
print("beta_y: ", lf.beta_y, " alpha_y: ", lf.alpha_y, " vertical tune: ", lf.psi_y/(2*np.pi), file=logger)

if myrank == 0:
    write_elems_pickle(lattice)

# 2 linac pulses per booster injection
linac_charge_per_pulse = 0.5*opts.booster_bunch_charge/opts.num_linac_injections
print("injecting {} linac pulses of {} each".format(opts.num_linac_injections, linac_charge_per_pulse), file=logger)

if opts.linac_beam == "realistic":
    linac_covariance = np.load('linac_covariance.npy')
elif opts.linac_beam == "cdr":
    linac_covariance = np.zeros((6,6),dtype='d')
    linac_covariance[0,0] = opts.linac_rmsx**2
    linac_covariance[0,1] = (-opts.linac_alpha_x/opts.linac_beta_x) * opts.linac_rmsx**2
    linac_covariance[1,0] = linac_covariance[0,1]
    linac_covariance[1,1] = ((1 + opts.linac_alpha_x**2)/opts.linac_beta_x**2) * opts.linac_rmsx**2

    linac_covariance[2,2] = opts.linac_rmsy**2
    linac_covariance[2,3] = (-opts.linac_alpha_y/opts.linac_beta_y) * opts.linac_rmsy**2
    linac_covariance[3,2] = linac_covariance[2,3]
    linac_covariance[3,3] = ((1 + opts.linac_alpha_y**2)/opts.linac_beta_y**2) * opts.linac_rmsy**2

    linac_covariance[4,4] = opts.linac_pulse_rms_phase * bucket_length/(2*np.pi)
    linac_covariance[5,5] = opts.linac_pulse_rms_dpop**2
else:
    linac_covariance = np.eye(6, dtype='d')

injector = linac_injector(linac_covariance, rf_frequency, opts.linac_rf_freq, opts.seed, commxx)

# Inject along an ellipse
painting = painting.Painting()

# skip spectator particles for now

# create the bunch
bunch = synergia.bunch.Bunch(refpart, 0, 0.0, commxx)
bunch.set_z_period_length(bucket_length)
bunch.set_bucket_index(0)

# create the loss diagnostics
aperture_diag = synergia.lattice.Diagnostics_loss("aperture_diagnostics.h5", "aperture")
aperture_diag.set_bunch(bunch)
zcut_diag = synergia.lattice.Diagnostics_loss("zcut_diagnostics.h5", "zcut")
zcut_diag.set_bunch(bunch)

lattice.add_loss_diagnostics(aperture_diag)
lattice.add_loss_diagnostics(zcut_diag)

# save charge from lost particles                                                                      
deposited_charge = synergia.lattice.Lattice_diagnostics(lattice,
                                                        "deposited_charge_injection.h5",
                                                        "deposited_charge")

# Now create the stepper

# choose stepper based on whether space charge is enabled
if opts.spacecharge == "none":
    stepper = synergia.simulation.Independent_stepper(lattice, 1, 1)
    print("Space charge not enabled, using Independent stepper 1/turn", file=logger)
elif opts.spacecharge == "2d":
    if opts.comm_divide:
        sc_comm = synergia.utils.Commxx_divider(opts.comm_divide, False)
    else:
        sc_comm = synergia.utils.Commxx(True)

    sc_grid = [opts.gridx, opts.gridy, opts.gridz]
    print("Using 2d open hockney solver, grid: {}x{}x{}".format(opts.gridx, opts.gridy, opts.gridz), file=logger)
    sc_operator = synergia.collective.Space_charge_2d_open_hockney(sc_comm, sc_grid)
    # for space charge, order=1 is sufficient
    stepper = synergia.simulation.Split_operator_stepper(lattice, 1, sc_operator, opts.steps)
    print("    stepper {} space charge kicks/turn".format(opts.steps), file=logger)
elif opts.spacecharge == "3d":
    if opts.comm_divide:
        sc_comm = synergia.utils.Commxx_divider(opts.comm_divide, False)
    else:
        sc_comm = synergia.utils.Commxx(True)

    sc_grid = [opts.gridx, opts.gridy, opts.gridz]
    print("Using 3d open hockney solver, grid: {}x{}x{}".format(opts.gridx, opts.gridy, opts.gridz), file=logger)

    # full signature for 3d_open_hockney constructor is
    # comm, grid, long_kicks, z_periodic, period, grid_entire_period,
    # nsigma
    #coll_operator = synergia.collective.Space_charge_3d_open_hockney(sc_comm, grid, opts.long_kicks, False, 0.0, False, opts.nsigma)
    sc_operator = synergia.collective.Space_charge_3d_open_hockney(sc_comm, sc_grid)
    # for space charge, order=1 is sufficient
    stepper = synergia.simulation.Split_operator_stepper(lattice, 1, sc_operator, opts.steps)
    print("    stepper {} space charge kicks/turn".format(opts.steps), file=logger)
elif opts.spacecharge == "rectangular":
    if opts.comm_divide:
        sc_comm = synergia.utils.Commxx_divider(opts.comm_divide, False)
    else:
        sc_comm = synergia.utils.Commxx(True)

    sc_grid = [opts.gridx, opts.gridy, opts.gridz]
    print("Using rectangular solver, grid: {}x{}x{}".format(opts.gridx, opts.gridy, opts.gridz), file=logger)

    # full signature for rectangular constructor is
    # comm, pipesize, grid, equally_spaced
    
    pipesizex = 0.0254 * (np.array(fmag_vertices)[:,0].max() -
                          np.array(fmag_vertices)[:,0].min())
    pipesizey = 0.0254 * (np.array(dmag_vertices)[:,1].max() -
                          np.array(dmag_vertices)[:,1].min())
    pipesizez = lattice.get_length()/opts.harmon
    pipesize = [pipesizex, pipesizey, pipesizez]

    sc_operator = synergia.collective.Space_charge_rectangular(sc_comm, pipesize, sc_grid, False)

    # for space charge, order=1 is sufficient
    stepper = synergia.simulation.Split_operator_stepper(lattice, 1, sc_operator, opts.steps)
    print("    stepper {} space charge kicks/turn".format(opts.steps), file=logger)

# redo the rf frequency tuning
# extract useful parameters
stepper.get_lattice_simulator().register_closed_orbit()

# register_closed_orbit() works for CHEF.  For libff, we need to do
stepper.get_lattice_simulator().tune_circular_lattice(1.0e-6)

bunch_simulator = synergia.simulation.Bunch_simulator(bunch)

tracks_diag_series = 0

bunch_simulator.add_per_turn(synergia.bunch.Diagnostics_full2("diag.h5"))
bunch_simulator.add_per_turn(synergia.bunch.Diagnostics_particles("inj_particles.h5"))

#bunch_simulator.add_per_turn(synergia.bunch.Diagnostics_bulk_track("tracks_{:04}.h5".format(0), opts.turn_tracks))
#bunch_simulator.add_per_turn(synergia.bunch.Diagnostics_bulk_track("tracks_{:04}.h5".format(0), opts.turn_tracks))

propagator = synergia.simulation.Propagator(stepper)
propagator.set_checkpoint_period(opts.checkpointperiod)

# main loop for painting
for inj_turn in range(painting.N):
    # get the hp_scale and vp_scale for injecting to this turn
    print('Injection turn ', inj_turn, file=logger)
    xpos, ypos = painting.pos(inj_turn)
    hp_scale, vp_scale = painting.turn_to_hvpscale(inj_turn)
    print('    injection position: {:.8f}, {:.8f}'.format(xpos, ypos), file=logger)
    print('    hp_scale: {:.7f}, vp_scale: {:.7f}'.format(hp_scale, vp_scale), file=logger)

    tuned_lattice_madx = booster_lattice2.read_booster(HP_scale=hp_scale, VP_scale=vp_scale)
    reader = synergia.lattice.MadX_reader()
    reader.parse(tuned_lattice_madx)
    tuned_lattice_orig = reader.get_lattice('booster_foil')
    del reader

    tuned_lattice = fixup_elements(tuned_lattice_orig, dogs_to_drifts=False)

    # tuned_lattice_simulator = synergia.simulation.Lattice_simulator(tuned_lattice, 1)
    # # get closed orbit which should match injection position
    # clorbit = tuned_lattice_simulator.get_closed_orbit()
    # print('    closed orbit: ', clorbit, file=logger)

    set_painting_kickers(stepper.get_lattice_simulator().get_lattice(), tuned_lattice)

    if myrank == 0:
        synergia.utils.write_lsexpr_file(stepper.get_lattice_simulator().get_lattice().as_lsexpr(),
                                         "booster_turn_{:04d}.lsx".format(inj_turn))
        # f = open('booster_turn_{:04d}.txt'.format(inj_turn), 'w')
        # print(lattice.as_string(), file=f)
        # f.close()

    # create linac pulse
    newbunch = injector.bunchlet(refpart, opts.linac_macroparticles, linac_charge_per_pulse, logger)

    newbunchmeans = synergia.bunch.Core_diagnostics.calculate_spatial_mean(newbunch)
    newbunchstds = synergia.bunch.Core_diagnostics.calculate_spatial_std(newbunch, newbunchmeans)
    print("    new bunch stats: xmean: {} ymean: {}".format(newbunchmeans[0], newbunchmeans[1]), file=logger)
    print("    xstd: {}, ystd: {}".format(newbunchstds[0], newbunchstds[1]), file=logger)

    bunch.inject(newbunch)
    print("    bunch particles: {}".format(bunch.get_total_num()), file=logger)
    
    del newbunch
    #del tuned_lattice_simulator
    del tuned_lattice
    del tuned_lattice_orig

    # crummy hand calculation here.  track 128 out of each 2048 particles
    # injected.  Choose 64 particles on each of the middle of the last
    # injection so I get both bunchlets. (these numbers work for 2 bunches
    # of 1024 particles each)
    bunch_simulator.add_per_turn(synergia.bunch.Diagnostics_bulk_track("tracks_{:04}.h5".format(inj_turn), 128, bunch.get_total_num()-(1024+64)))

    if inj_turn%10 == 0 or inj_turn == painting.N-1:
        propagator.set_checkpoint_period(1)
    else:
        propagator.set_checkpoint_period(100)
    propagator.propagate(bunch_simulator, 1, 1, opts.verbosity)

    deposited_charge.update_and_write()

    #  end of injection loop


# do extra propagation after injection

propagator.set_checkpoint_period(40)
propagator.propagate(bunch_simulator, opts.extra_turns, 0, opts.verbosity)

deposited_charge.update_and_write()
