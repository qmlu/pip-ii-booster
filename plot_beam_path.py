#!/usr/bin/env python3
import sys, os
import h5py
import numpy as np
import matplotlib.pyplot as plt
import pickle

f = open('elems.pickle', 'rb')
elems = pickle.load(f)
f.close()
print('len(elems): ', len(elems))

h5basic = h5py.File('basic.h5', 'r')
s = h5basic.get('s')[()]
s_n = np.array(h5basic.get('s_n'))[()]
mean = h5basic.get('mean')[()]
std = h5basic.get('std')[()]
npart = h5basic.get('num_particles')[()]

print('mean.shape: ', mean.shape)
print('s_n.shape: ', s_n.shape)
print('npart.shape: ', npart.shape)

print('nsteps: ', s.shape[0])

for i in range(s_n.shape[0]-1):
    print(elems[i][0], ': ', npart[i+1])

plt.figure()
plt.suptitle('beam trajectories')
plt.subplot(311)
plt.plot(s_n, mean[:, 0], label='x trajectory')
plt.ylabel('x [m]')
plt.legend(loc='best')
plt.subplot(312)
plt.plot(s_n, mean[:, 2], label='y trajectory')
plt.ylabel('y [m]')
plt.legend(loc='best')
plt.subplot(313)
plt.plot(s_n, npart, label='num particles')
plt.legend(loc='best')

plt.xlabel('x [m]')
plt.ylabel('particles')

plt.show()
