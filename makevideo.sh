#!/usr/bin/bash

# found a bunch of options to help with display
# apparently pixel format needs to be yuv420p to play correctly
# if you to slow down by factor of 3, add video filter setpts=3.0*PTS

ffmpeg -i x-y-%04d.png -filter:v format=yuv420p -c:v libx264 -preset slow -profile:v main -crf 20 x-y-injection.mp4

ffmpeg -i x-xp-%04d.png -filter:v format=yuv420p -c:v libx264 -preset slow -profile:v main -crf 20 x-xp-injection.mp4

ffmpeg -i y-yp-%04d.png -filter:v format=yuv420p -c:v libx264 -preset slow -profile:v main -crf 20 y-yp-injection.mp4

ffmpeg -i z-zp-%04d.png -filter:v format=yuv420p -c:v libx264 -preset slow -profile:v main -crf 20 z-zp-injection.mp4
