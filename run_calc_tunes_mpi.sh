#!/bin/bash
#SBATCH -N 1
##SBATCH -p cpu_gce
#SBATCH -q regular
#SBATCH --mail-user=egstern@fnal.gov
#SBATCH --mail-type=ALL
#SBATCH -t 08:00:00
#SBATCH -A accelsim
#SBATCH --output=calc_tunes-%j.out
#SBATCH --error=calc_tunes-%j.err

. /work1/accelsim/egstern/syn2-devel-pre3/install/bin/setup.sh

# for f in $( ls tracks_0[0-9][0-9][0-9].h5 )
# do
#     python $WORKDIR/pip-ii-booster/undouble.py ${f}
# done

for f in $( ls tracks_ud_0[0-9][0-9][0-9].h5 )
do
    srun -n 16 --mpi=pmix python3 $WORKDIR/pip-ii-booster/calc_tunes_onefile_h5py.py ${f}
done
