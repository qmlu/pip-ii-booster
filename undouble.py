#!/usr/bin/env python3

import sys, os
import numpy as np
import h5py
import re

for f in sys.argv[1:]:
    print('Reading file ', f)
    h5a = h5py.File(f, 'r')
    fparts = os.path.splitext(f)
    outpat = fparts[0]
    pat = '(.*)_(\d\d\d\d)'
    rep = '\\1_ud_\\2'
    outfile = re.sub(pat, rep, outpat) + fparts[1]
    print('Writing undoubled file to ', outfile)
    h5b = h5py.File(outfile, 'w')

    h5b['charge'] = h5a.get('charge')[()] # scalar
    h5b['mass'] = h5a.get('mass')[()] # scalar
    # get length of the by-turn entries
    nent = h5a.get('repetition').shape[0]
    # the last 1025 entries are the 1024 turns, but the first of that 
    # duplicates the last entry of the last injection turn, so remove it
    # save the remaining
    savelast = nent-1024
    savefirst = savelast-1

    # start moving things over
    pz = h5a.get('pz')[()]
    h5b['pz'] = np.hstack((pz[1:savefirst:2], pz[savelast:]))

    repetition = h5a.get('repetition')[()]
    h5b['repetition'] = np.hstack((repetition[1:savefirst:2], repetition[savelast:]))

    s = h5a.get('s')[()]
    h5b['s'] = np.hstack((s[1:savefirst:2], s[savelast:]))

    s_n = h5a.get('s_n')[()]
    h5b['s_n'] = np.hstack((s_n[1:savefirst:2], s_n[savelast:]))

    track_coords = h5a.get('track_coords')[()]
    h5b['track_coords'] = np.concatenate((track_coords[1:savefirst:2,:,:], track_coords[savelast:, :, :]), axis=0)

    h5b.close()
    h5a.close()

    
