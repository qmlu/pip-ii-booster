#!/usr/bin/env python

from math import pi
import synergia_workflow

opts = synergia_workflow.Options("pip2_injection")

# options file for running the booster taken from macridin cxx_booster

opts.add("map_order", 1, "map order for map calculations")
opts.add("label_saveload_bunch", "", "label from which to save or load bunch")
opts.add("chef_map", False, "use chef maps")
opts.add("chef_propagate", False, "use chef propagation")
opts.add("libff", True, "use libff propagation")
opts.add("libff_norf", False, "use libff propagation except for RF cavities")
opts.add("bpms", True, "whether bpms are present")
opts.add("impedance", False, "whether impedance calculation is active")
opts.add("spacecharge", "none", "space charge solver [none, 2d, 3d, rectangular]")
opts.add("gridx", 32, "space charge x grid size")
opts.add("gridy", 32, "space charge y grid size")
opts.add("gridz", 256, "space charge z grid size")
opts.add("steps", 6*24, "number of space charge kicks/turn")
opts.add("if_aperture", True, "whether to enable apertures on F and D magnets")
opts.add("aperture_f", 0.021, "size of aperture for F magnet")
opts.add("aperture_d", 0.029, "size of aperture for D magnet")
opts.add("aperture_l", 0.05715, "size of longitudinal aperture")
opts.add("apertures", True, "enable apertures")

opts.add("grid_ref_distance", 0.01, "grid ref distance")
#opts.add("xrms", 0.005, "x RMS")
#opts.add("xrms", 0.1577, "x RMS")  # input_options file
#opts.add("yrms", 0.006, "y RMS")
#opts.add("yrms", 0.00337, "y RMS") # input_options file
#opts.add("zrms", 0.4, "z RMS")
#opts.add("zrms", 1.5, "z RMS") # input_options file
opts.add("xoffset", 0.0, "x offset")
opts.add("yoffset", 0.0, "y offset")
opts.add("zoffset", 0.0, "z offset")

opts.add("num_steps", 24*6, "number of steps")
opts.add("steps_per_fmag", 1, "number of steps/f magnet")
opts.add("steps_per_dmag", 1, "number of steps/d magnet")
opts.add("num_steps_else", 24*4, "number of steps outside f and d magnets")
opts.add("num_bunches", 1, "number of bunches")
opts.add("bunch_periodic", True, "whether bunch is periodic in z")
opts.add("macroparticles", 10000, "number of macroparticles/bunch")
opts.add("realparticles", 1.2e10, "number of real particles/bunch")
opts.add("seed", 13, "random seed")
opts.add("load_bunch", False, "whether to load bunch from file")
opts.add("saved_normal_form", "normal_form.xml", "the fucking file")
opts.add("print_lattice", False, "whether to print lattice")
opts.add("turn_tracks", 20000, "number of tracks to save if nonzero")
opts.add("phase_space", 1, "whether to save phase space diagnostics")
opts.add("turn_period", 1, "??")
opts.add("checkpointperiod", 5000, "period between checkpoints")
opts.add("maxturns", 3000, "maximum number of turns per run")
opts.add("turns", 1000, "total number of turns")
opts.add("extra_turns", 1024, "extra turns to propagate after injection")
opts.add("rf_voltage", 0.2, "total voltage of rf cavity (MV)")
opts.add("harmon", 84, "harmonic number")
opts.add("tunes_and_chroms", True, "print tunes and chromaticities")

opts.add("particle_period", 1, "period to dump particles")
opts.add("verbosity", 1, "how much chatter")
opts.add("comm_divide", 32, "communicator size for space charge calculation")
opts.add("spc_tuneshift", True, "calculate space charge tuneshift diagnostic")
opts.add("read_sextupoles", True, "Read sextupole settings from file")
opts.add("adjust_chromaticity", False, "adjust chromaticity")
opts.add("adjust_tunes", False, "adjust tunes")
opts.add("tune_h", 0.75, "adjust h_tune setting")
opts.add("tune_v", 0.84, "adjust v_tune setting")
opts.add("chrom_h", -17.0, "adjust h_chrom setting")
opts.add("chrom_v", -9.0, "adjust v_chrom setting")
opts.add("wakefile_f", "Fwake.dat", "wake file for F")
opts.add("wakefile_d", "Dwake.dat", "wake file for D")
opts.add("waketype", "XLXTYLYTZpp", "wake type")
opts.add("registered_turns", 15, "number of turns included in wake file")

opts.add("injection_energy", 800, "Energy at injection [400|800]", int)
opts.add("linac_beam", "realistic", "linac beam type [realistic, pencil, cdr]")
opts.add("linac_beta_x", 2.98, "beta_x from the linac")
opts.add("linac_alpha_x", -0.046, "alpha_x from the linac")
opts.add("linac_beta_y", 9.67, "beta_y from the linac")
opts.add("linac_alpha_y", -0.014, "alpha_y from the linac")
opts.add("num_linac_injections", 285, "number of linac injections")
opts.add("booster_bunch_charge", 6.7e12/81, "total charge in the booster bunch after injection")
opts.add("linac_macroparticles", 1024, "number of macroparticles per linac pulse")
opts.add("linac_pulse_rms_phase", 1.9e-2, "rms phase length of linac pulse")
opts.add("linac_pulse_rms_dpop", 2.2e-4, "rms dp/p of linac pulse")
opts.add("linac_rmsx", 0.0007, "linac x rms")
opts.add("linac_rmsy", 0.0013, "linac y rms")
opts.add("linac_rf_freq", 162.5e6, "linac rf frequency [Hz]")

opts.add("on_momentum", True, 'Whether injection is on-momentum or off-momentum')
# on-momentum injection has no momentum offset and and injections beam
# in phase windows (-0.7\pi,0.15\pi) and (0.15\pi, 0.7\pi)

# off-momentum injection offsets the injection momentum dp/p to 7e-4
# and gates injections within phase window \pm 0.55 \pi. The x injection
# location is offset by -1.5mm.  (original CDR scheme)


job_mgr = synergia_workflow.Job_manager("pip2_injection.py", opts, ['painting.py', 'tfs.py', 'booster_lattice2.py', 'booster_fixup2.py', 'write_elems_pickle.py', 'booster_PIP2_header.madx', 'linac_covariance.npy'], ['pip-ii-booster-madx-files'])

