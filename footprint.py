#!/usr/bin/env python

import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import pylab
#~ import synergia
#~ import density_plot
import time
from tune_diagram import tune_diagram
import math
import sys
import os
#import xtract

show_bare_tune = True

def vals2bins(vals,multiplier):
    u = np.unique(vals)
    num = u.shape[0]
    diffs = u[1:num] - u[0:(num-1)]
    step = min(diffs)*multiplier
    #step = 0.001
    #step = 0.005
    step = 0.005
    #step = 0.0071
    bins = np.arange(u[0]-step/2.0,u[num-1]+step/2.0,step)
    #   WTF!!!  you go through all this fucking work and then throw it away?
    #bins = np.arange(0.35,0.5, step)
    print("step for bins is ", step)
    print("Number bins: ", len(bins))
    return bins

def do_filter(otunesx,otunesy,xmin,xmax,ymin,ymax):
        num = 0
        onum = otunesx.shape[0]
        ltunesx = np.zeros([onum],'d')
        ltunesy = np.zeros([onum],'d')
        for i in range(0,onum):
            x = otunesx[i]
            y = otunesy[i]
            if ((x >= xmin) and (x<=xmax) and (y >= ymin) and (y<=ymax)):
                ltunesx[num] = x
                ltunesy[num] = y
                num += 1
        tunesx = ltunesx[0:num]
        tunesy = ltunesy[0:num]
        print("kept",num,"/",onum)
        return tunesx,tunesy

def filter_ranges_by_sigma(otunesx,otunesy,half_sigma_size):
        xmin = otunesx.mean() - 2*half_sigma_size*otunesx.std()
        xmax = otunesx.mean() + 2*half_sigma_size*otunesx.std()
        ymin = otunesy.mean() - 2*half_sigma_size*otunesy.std()
        ymax = otunesy.mean() + 2*half_sigma_size*otunesy.std()
        print("filter",2*half_sigma_size,"sigma:", end='')
        tunesx,tunesy = do_filter(otunesx,otunesy,xmin,xmax,ymin,ymax)
        if ( (len(np.unique(tunesx))<2) or (len(np.unique(tunesy))<2) ):
            print("Filter reduced footprint to single point. Removing filter.")
            xmin = otunesx.min()
            xmax = otunesx.max()
            ymin = otunesy.min()
            ymax = otunesy.max()
            tunesx, tunesy = otunesx,otunesy
        return tunesx,tunesy,xmin,xmax,ymin,ymax

def filter_ranges_by_min_max(otunesx,otunesy,min1,max1,min2,max2 ):
        xmin = min1
        xmax = max1
        ymin = min2
        ymax = max2
        tunesx,tunesy = do_filter(otunesx,otunesy,xmin,xmax,ymin,ymax)
        if ( (len(np.unique(tunesx))<2) or (len(np.unique(tunesy))<2) ):
            print("Filter reduced footprint to single point. Removing filter.")
            xmin = otunesx.min()
            xmax = otunesx.max()
            ymin = otunesy.min()
            ymax = otunesy.max()
            tunesx, tunesy = otunesx,otunesy
        return tunesx,tunesy,xmin,xmax,ymin,ymax
            
# directory here mean tunes npy file
def doplot(tunes_file,plttitle,resonance_order,label,half_sigma_size,colorbar=True,footprint=True,nux=None, nuy=None):
    xmin = 0.5
    xmax = 0.8
    ymin = 0.5
    ymax = 0.8
    if footprint:
        tunes = np.load(tunes_file, allow_pickle=True)[()]

        # the file contains a dict indexed by particle id.  Each entry has a
        # tuple of [track-starts], [x-tunes], [y-tunes], [z-tunes].  For the
        # purposes of this script, we will use only the first entry of each
        # particle

        # the tunes from the file are in the range [0, 0.5) but that is
        # really the alias of [0.5, 1.0) so subtract from 1.0
        otunesx = np.array([(1.0 - tunes[pid][1][0]) for pid in tunes])
        otunesy = np.array([(1.0 - tunes[pid][2][0]) for pid in tunes])
        #otunesx = 1-np.load("%s/xtunes.npy" % directory);
        #otunesy = 1-np.load("%s/ytunes.npy"% directory);

        print("read ", len(otunesx), " x tunes, min: ", otunesx.min(), " max: ", otunesx.max())
        print("read ", len(otunesy), " y tunes, min: ", otunesy.min(), " max: ", otunesy.max())
        
        min1=0.5
        max1=0.9
        min2=0.5
        max2=0.9
        tunesx,tunesy,xmin,xmax,ymin,ymax = filter_ranges_by_min_max(otunesx,otunesy,min1,max1,min2,max2)
        print("egs: in doplot: len(otunesx), len(otunesy), half_sigma_size: ", len(otunesx), len(otunesy), half_sigma_size)
        print("egs: in doplot: len(tunesx): ", len(tunesx))
        print("egs: in doplot: len(tunesy): ", len(tunesy))
        xbins = vals2bins(tunesx,1)
        print("xbins from: ", xbins.min()," to: ", xbins.max(), ", mean: ", xbins.mean(),", std: ", xbins.std())
        #print "xbins: ", xbins
        ybins = vals2bins(tunesy,1)
        print("ybins from: ", ybins.min()," to: ", ybins.max(), ", mean: ", ybins.mean(),", std: ", ybins.std())
        #print "ybins: ", ybins
        H,xedges,yedges = np.histogram2d(tunesx,tunesy,bins=(xbins,ybins))
        print("xedges: min: ", xedges.min(),", max: ",xedges.max())
        print("yedges: min: ", yedges.min(),", max: ",yedges.max())
        #print "xedges: ", xedges
        #print "yedges: ", yedges
        print("after filter_ranges: xmin: ", xmin, " xmax: ", xmax)
        print("after filter_ranges: ymin: ", ymin, " ymax: ", ymax)
        for i in range(0,H.shape[0]):
            for j in range(0,H.shape[1]):
                if H[i,j] > 0:
                    H[i,j] = math.log(H[i,j])
        X,Y = pylab.meshgrid(xedges,yedges)
        #plt.pcolor(X,Y,H.transpose(),alpha=0.9, vmin=0.1, vmax=1.2)
        plt.pcolor(X,Y,H.transpose(),alpha=0.9, cmap='plasma')

        covtunes = np.cov(tunesx, tunesy)
        meanx = tunesx.mean()
        meany = tunesy.mean()
        print("meanx, meany: ", meanx, meany)
        npnts = 10000
        theta = np.linspace(0.0, 2.0*np.pi, npnts)
        uandv = np.vstack((np.cos(theta), np.sin(theta)))
        ltunes,vtunes = np.linalg.eig(covtunes)
        xandy = np.dot(vtunes, uandv)
        vtunes[0,:] *= np.sqrt(ltunes[0])
        vtunes[0,:] += meanx
        vtunes[1,:] *= np.sqrt(ltunes[1])
        vtunes[1,:] += meany
        # is this the fucking white line?
        #plt.plot(vtunes[0,:], vtunes[1,:],'w')

    if colorbar:
        cb = plt.colorbar()
        cb.set_label('log density [arbitrary units]')
    
    plt.title(plttitle)
    plt.xlabel(r'$\nu_x$',fontsize=20)
    plt.ylabel(r'$\nu_y$',fontsize=20)

    htune, vtune = (0.77546, 0.81126)
    if nux and nuy:
        #barex = [htune]
        barex = [nux]
        #barey = [vtune]
        barey = [nuy]
        if show_bare_tune:
            plt.scatter(barex,barey,s=80,c='y',marker='o')
            plt.annotate(label, xy=(barex[0],barey[0]),color='yellow',
                         xytext=(0,5),textcoords = 'offset points')

    #~ tuneshift = 0.08
    #~ calcx = [barex[0]-tuneshift]
    #~ calcy = [barey[0] - tuneshift]
    #~ plt.scatter(calcx,calcy,s=80,c='y',marker='o')

    #~ plt.annotate(label, xy=(barex[0],barey[0]),color='white',
        #~ xytext=(-150, -20), textcoords='offset points',
        #~ arrowprops=dict(arrowstyle="fancy",
                                    #~ connectionstyle="angle3,angleA=15,angleB=90",
                                    #~ fc='white',ec='none')
        #~ )

    # XXXX don't lose the tune diagram!
    tune_diagram(resonance_order,['black','red','cyan','white','orange','green'])

    axes = plt.gca()

    # apparently set_axis_bgcolor -> set_facecolor
    #axes.set_axis_bgcolor((0,0,0.5))
    axes.set_facecolor((0,0,0.5))

    #~ plt.axis([xmin,xmax,ymin,ymax])
    #new_axis = [htune-0.015,htune+0.005,vtune-0.015,vtune+0.005]
    # set range for plots so I can get two plots with same scale
    # previously new_axis = [xmin, xmax, ymin, ymax]
    #(xlow, xhigh, ylow, yhigh) = (0.32,0.44,0.31, 0.43)


    #(xlow, xhigh, ylow, yhigh) = (0.5, 0.9, 0.5, 0.9)
    (xlow, xhigh, ylow, yhigh) = (0.55, 0.80, 0.6, 0.85)
    new_axis = [xlow, xhigh, ylow, yhigh]
    # WTF is going on here??
    #new_axis = round(250*np.array(new_axis))
    #new_axis  = np.array(new_axis,'d')/250.0



    #~ eps = 0.0001
    #~ new_axis[0] -= eps
    #~ new_axis[2] -= eps
    #~ new_axis[1] += eps
    #~ new_axis[3] += eps
    print(new_axis)
    plt.axis('equal')
    plt.axis(new_axis)
if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("usage: python footprint.py <tunes-file> <plot-title> barex barey")
        sys.exit(20)

    tunes_file = sys.argv[1]
#    resonance_order = int(sys.argv[2])
    resonance_order = 3 # not plotting resonances at the moment
#    label = sys.argv[2]


    plttitle = ""
    if len(sys.argv) > 1:
        plttitle = sys.argv[2]

    barex = None
    barey = None
    if len(sys.argv) > 2:
        barex = float(sys.argv[3])
        barey = float(sys.argv[4])
    
    label = "bare tune"
    half_sigma_size = 1.5
    doplot(tunes_file,plttitle,resonance_order,label,half_sigma_size, nux=barex, nuy=barey)

    plt.savefig("footprint.png", dpi=150)
    plt.savefig("footprint.eps")
    plt.savefig("footprint.svg")
    plt.show()

    sys.exit(0)
    #~ plt.savefig("plotfootprintramp3.png",dpi=150)
    if len(sys.argv)> 2:
        plt.savefig(sys.argv[2],dpi=150)
