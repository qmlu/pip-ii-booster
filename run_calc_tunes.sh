#!/bin/bash

. /work1/accelsim/egstern/syn2-devel-pre3/install/bin/setup.sh

for f in $( ls tracks_0[0-9][0-9][0-9].h5 )
do
    echo "undouble ${f}"    
    python3 $WORKDIR/pip-ii-booster/undouble.py ${f}
done

for f in $( ls tracks_ud_0[0-9][0-9][0-9].h5 )
do
    echo "calculating tunes ${f}"
    python3 $WORKDIR/pip-ii-booster/calc_tunes_onefile_h5py.py ${f}
done
