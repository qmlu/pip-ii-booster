#!/usr/bin/env python3
from __future__ import print_function

import sys, os
import numpy as np
import synergia

DEBUG = False

########################################################################

# fix up elements
def fixup_elements(orig_lattice, rbends_to_sbends=True, zero_angle_bends_to_drifts=True, dogs_to_drifts=True, kill_sept03=True):

    if callable(hasattr(orig_lattice, 'get_element_adaptor_map_sptr')):
        lattice = synergia.lattice.Lattice(orig_lattice.get_name(), orig_lattice.get_element_adaptor_map_sptr())
    else:
        lattice = synergia.lattice.Lattice(orig_lattice.get_name())


    for elem in orig_lattice.get_elements():

        new_elem_here = False

        if zero_angle_bends_to_drifts and (elem.get_type() == "rbend" or elem.get_type() == "sbend"):
            angle = 0.0
            if elem.has_double_attribute("angle"):
                angle = elem.get_double_attribute("angle")
            if abs(angle) < 1.0e-8 :
                new_elem = synergia.lattice.Lattice_element("drift", elem.get_name())
                new_elem.set_double_attribute("l", elem.get_length())
                new_elem_here = True
                if DEBUG:
                    print("old elem: ", elem.as_string())
                    print("new elem: ", new_elem.as_string())
                elem = new_elem

        if dogs_to_drifts and elem.get_name()[0:3] == "dog":
            new_elem = synergia.lattice.Lattice_element("drift", elem.get_name())
            s_attributes = elem.get_string_attributes()
            d_attributes = elem.get_double_attributes()
            for s in s_attributes.keys():
                new_elem.set_string_attribute(s, s_attributes[s])
            for d in d_attributes.keys():
                if (d=='l'):
                    new_elem.set_double_attribute(d, d_attributes[d])
            if DEBUG:
                print("old dog: ", elem.as_string())
                print("new dog: ", new_elem.as_string())
            elem = new_elem

        if kill_sept03 and elem.get_name() == "sept03":
            new_elem = synergia.lattice.Lattice_element("drift", "sept03")
            new_elem.set_double_attribute("l", elem.get_length())
            elem = new_elem

        if rbends_to_sbends and elem.get_type() == "rbend":
            new_elem_here = True
            new_elem = synergia.lattice.Lattice_element("sbend", elem.get_name())
            s_attributes = elem.get_string_attributes()
            d_attributes = elem.get_double_attributes()
            for s in s_attributes.keys():
                new_elem.set_string_attribute(s, s_attributes[s])
            for d in d_attributes.keys():
                new_elem.set_double_attribute(d, d_attributes[d])
            ang = 0.0
            if elem.has_double_attribute("angle"):
                ang = elem.get_double_attribute("angle")
            length = elem.get_double_attribute("l")
            arclength = ang*length/(2.0*np.sin(ang/2.0))
            new_elem.set_double_attribute("l", arclength)
            new_elem.set_double_attribute("e1", ang/2.0)
            new_elem.set_double_attribute("e2", ang/2.0)
            if DEBUG:
                print("old rbend: ", elem.as_string())
                print("new sbend: ", new_elem.as_string())
            elem = new_elem

        lattice.append(elem)

    # end loop over elem in orig_lattice
    lattice.set_reference_particle(orig_lattice.get_reference_particle())
    return lattice

########################################################################

# set the RF cavity voltage
def set_rfcavity_voltage(lattice, total_voltage):
    rfcnt = 0
    for elem in lattice.get_elements():
        if elem.get_type() == "rfcavity":
            rfcnt = rfcnt + 1
            #print(elem.as_string())                                                       #print("Found {} RF cavities".format(rfcnt))

    for elem in lattice.get_elements():
        if elem.get_type() == "rfcavity":
            elem.set_double_attribute("volt", total_voltage/rfcnt)

########################################################################

def kill_muldg(lattice):
    for elem in lattice.get_elements():
        if elem.get_name()[0:5] == "muldg":
            elem.set_vector_attribute("knl", [0.0, 0.0, 0.0])

########################################################################

def use_maps(lattice):
    for elem in lattice.get_elements():
        elem.set_string_attribute("extractor_type", "chef_mixed")

########################################################################

def use_chef(lattice):
    for elem in lattice.get_elements():
        elem.set_string_attribute("extractor_type", "chef_propagate")

########################################################################

def use_libff(lattice):
    for elem in lattice.get_elements():
        elem.set_string_attribute("extractor_type", "libff")

########################################################################

def use_libff_except_rf(lattice):
    for elem in lattice.get_elements():
        if elem.get_type() != "rfcavity":
            elem.set_string_attribute("extractor_type", "libff")
        else:
            elem.set_string_attribute("extractor_type", "chef_propagate")

########################################################################
########################################################################
########################################################################
########################################################################

# mark BMA regions



#    On 8/3/20 12:06 PM, Salah J Chaurize wrote:
#    Short BMA min aperture is 4.5 inches.  long BMA min aperture is 3.125 inches,
#    dogleg BMA 3.875i#n and Orbump BMA is 4.375in. kickers ceramic beam tubes are 2.25 x 2.375in
#    RF cavity 2.25in. These numbers are mostly from mechanical drawings particularly for BMAs.
#    -Salah

#    From: Kiyomi Seiya <kiyomi@fnal.gov> 
#    Sent: Monday, August 3, 2020 10:27 AM
#    To: Cheng-Yang Tan <cytan@fnal.gov>
#    Cc: Salah J Chaurize <chaurize@fnal.gov>; David E Johnson <dej@fnal.gov>
#    Subject: RE: Do you have a map of Booster apertures

#    I checked my note.  The collimator aperture is 4.5 inch, and 5 inch for S12.
#    I don't recall short and long have different apertures.

#    Kiyomi

def mark_short_bma(lattice):
    mark_bma(lattice, 'hs%02d', 'sss%02d', 4.5 * 0.0254/2)

def mark_long_bma(lattice):
    mark_bma(lattice, 'hl%02d', 'ssl%02d', 3.125 * 0.0254/2)


# add a "short_bma" to string attribute aperture_type
def mark_bma(lattice, start_pattern, end_pattern, radius):
    elem_names = [e.get_name() for e in lattice.get_elements()]
    all_elems = list(lattice.get_elements())

    # sector goes from 1 to 24
    for sector in range(1,25):
        firstname = start_pattern%sector
        lastname = end_pattern%sector
        if firstname not in elem_names:
            #raise RuntimeError("%s not present in element names"%firstname)
            print('Error: {} element not found... skipping...'.format(firstname))
            continue

        firstloc = elem_names.index(firstname)
        
        if lastname not in elem_names:
            raise RuntimeError("%s not present in element names"%lastname)

        lastloc = elem_names.index(lastname)

        # search backwards from firstname for first nondrift
        nondrift = False
        startbma = None
        curloc = firstloc
        #print("short BMA ", sector, " search begin starting at ", curloc-1, all_elems[curloc-1].get_name(), ":", all_elems[curloc-1].get_type())
        while not nondrift:
            if all_elems[curloc-1].get_type() == "drift":
                curloc = curloc - 1
            else:
                nondrift = True

        bma_start = curloc

        #print("short BMA ", sector, " starts found at element ", curloc, all_elems[curloc].get_name(), ":", all_elems[curloc].get_type())
        
        nondrift = False
        endbma = None
        curloc = lastloc + 1
        while not nondrift:
            if all_elems[curloc].get_type() == "drift":
                curloc = curloc + 1
            else:
                nondrift = True
        #print("BMA ", sector, " ends at element", curloc, all_elems[curloc].get_name(),":", all_elems[curloc].get_type())

        bma_end = curloc

        for idx in range(bma_start, bma_end):
            all_elems[idx].set_string_attribute("aperture_type", "circular")
            all_elems[idx].set_double_attribute("circular_aperture_radius", radius)


########################################################################
fmag_vertices = [(3.74,0.506), (-2.16, +1.09), (-2.16, -1.09), (3.74, -0.506)]
dmag_vertices = [(3.50,1.52), (-2.40, +0.901), (-2.40, -0.901), (3.50, -1.52)]

def set_apertures_fdmag(lattice):
    for elem in lattice.get_elements():
        if elem.get_name()[0:4] == "fmag":
            # focussing magnet
            elem.set_string_attribute("aperture_type", "polygon")
            elem.set_double_attribute("the_number_of_vertices", 4)
            # vertex coordinates in inches
            vertices = fmag_vertices
            for i, vtx in enumerate(vertices):
                elem.set_double_attribute("pax%d"%(i+1), vtx[0]*0.0254)
                elem.set_double_attribute("pay%d"%(i+1), vtx[1]*0.0254)
            # find distance from center to aperture edge
            # need the equation for the between (-2.54, 0.925) to (3.46, 0.925)
            # line that goes between (x1, y1) and (x2, y2)
            #  A = (y2-y1), B = -(x2-x1), C = -x1*(y2-y1) + y1*(x2-x1)
            # min_radius2 = C**2/(A**2 + B**2)
            A = vertices[1][1] - vertices[0][1]
            B = -(vertices[1][0] - vertices[0][0])
            C = -vertices[0][0]*(vertices[1][1] - vertices[0][1]) + vertices[0][1]*(vertices[1][0] - vertices[0][0])
            elem.set_double_attribute("min_radius2", C**2/(A**2 + B**2))
        elif elem.get_name()[0:4] == "dmag":
            # defocussing magnet
            elem.set_string_attribute("aperture_type", "polygon")
            elem.set_double_attribute("the_number_of_vertices", 4)
            # vertex coordinates in inches
            vertices = dmag_vertices
            for i, vtx in enumerate(vertices):
                elem.set_double_attribute("pax%d"%(i+1), vtx[0]*0.0254)
                elem.set_double_attribute("pay%d"%(i+1), vtx[1]*0.0254)
            # find distance from center to aperture edge
            # need the equation for the between (-2.54, 0.925) to (3.46, 0.925)
            # line that goes between (x1, y1) and (x2, y2)
            #  A = (y2-y1), B = -(x2-x1), C = -x1*(y2-y1) + y1*(x2-x1)
            # min_radius2 = C**2/(A**2 + B**2)
            A = vertices[1][1] - vertices[0][1]
            B = -(vertices[1][0] - vertices[0][0])
            C = -vertices[0][0]*(vertices[1][1] - vertices[0][1]) + vertices[0][1]*(vertices[1][0] - vertices[0][0])
            elem.set_double_attribute("min_radius2", C**2/(A**2 + B**2))
            

########################################################################

# RF cavity aperture 2.25in
rfcavity_radius = 2.25 * 0.0254/2

def set_rf_apertures(lattice):
    for elem in lattice.get_elements():
        if elem.get_type() == "rfcavity":
            elem.set_string_attribute("aperture_type", "circular")
            elem.set_double_attribute("circular_aperture_radius", rfcavity_radius)

########################################################################

# set a wide aperture in the injection region
def unmark_injection_region(lattice):
    big_radius = 20.0
    # the injection region uses horizontal kickers hs09-hs12 for horizontal
    # painting and vertical kickers vpk1-vpk4 for vertical painting.
    # The major excursion is in the vertical direction so unset apertures
    # between the vertical kicks

    first_name = "vpk1"
    last_name = "vpk4"

    elem_names = [e.get_name() for e in lattice.get_elements()]
    all_elems = list(lattice.get_elements())
    nelems = len(elem_names)

    firstloc = elem_names.index(first_name)
    lastloc = elem_names.index(last_name)

    if firstloc < lastloc:
        for idx in range(firstloc, lastloc+1):
            all_elems[idx].set_double_attribute("circular_aperture_radius", big_radius)
    else:
        # in fact I know that hs09 is near the end and hs12 is near the
        # beginning since the injection region is right at the start of
        # this lattice, so I have to wrap around

        for idx in range(firstloc, nelems):
            all_elems[idx].set_double_attribute("circular_aperture_radius", big_radius)
        for idx in range(0, lastloc+1):
            all_elems[idx].set_double_attribute("circular_aperture_radius", big_radius)


########################################################################

# From JFO 2021-11-02:
# MKS12  New long 12 kicker apertures
#   ( first three )  57.15mm H x 60.33 mm V  ,
#   last kicker 60.33 mm H x 57.15mm V  rectangular 

def set_apertures_mks12(lattice):
    mk_cnt = 0
    for elem in lattice.get_elements():
        if elem.get_name()[0:5] == "mks12":
            mk_cnt = mk_cnt + 1
            elem.set_string_attribute('aperture_type', 'rectangular')
            if mk_cnt < 4:
                # horizontal
                elem.set_double_attribute('rectangular_aperture_width', 57.15e-3)
                # then vertical
                elem.set_double_attribute('rectangular_aperture_height', 60.33e-3)
            elif mk_cnt == 4:
                # horizontal
                elem.set_double_attribute('rectangular_aperture_width', 60.33e-3)
                # then vertical
                elem.set_double_attribute('rectangular_aperture_height', 57.15e-3)
            else:
                return


########################################################################

# From JFO 2021-11-02:
# Injection absorber
# ABS   ROUND R= 45mm   radius

def set_apertures_abs(lattice):
    for elem in lattice.get_elements():
        if elem.get_name()[0:3] == "abs":
            elem.set_string_attribute('aperture_type', 'circular')
            elem.set_double_attribute('circular_aperture_radius', 45e-3)

########################################################################

# information from JFO 2021-10-08
# ORBUMPa_x
# MULBUMP_x
#
# apertures vertically    300 mm
#           horizontally   56 mm



def set_orbump_apertures(lattice):
    VBUMP_aperture = 300.0e-3
    HBUMP_aperture = 56e-3
    for elem in lattice.get_elements():
        name = elem.get_name()
        if name[0:8] == "orbumpa_" or name[0:8] == "mulbump_":
            elem.set_string_attribute('aperture_type', 'rectangular')
            elem.set_double_attribute('rectangular_aperture_height', VBUMP_aperture)
            elem.set_double_attribute('rectangular_aperture_width', HBUMP_aperture)
########################################################################

def set_apertures(lattice):
    set_apertures_fdmag(lattice)
    mark_short_bma(lattice)
    mark_long_bma(lattice)
    set_rf_apertures(lattice)
    set_apertures_mks12(lattice)
    set_apertures_abs(lattice)
    unmark_injection_region(lattice)
    set_orbump_apertures(lattice)

########################################################################

# Injection painting adjusts horizontal kickers HS{xx} where xx is
# 09, 10, 11, 12 by
# setting HP_scale which alters the current corresponding i_hs{xx} which,
# which sets the HKICK
# attribute in the kickers through files booster_PIP2_inj_bumps.dat into
# booster_PIP2.ele.
#
# The vertical painting uses VP_scale to set pk{x}_kick (x=1,2,3,4) variable
# which sets the KICK attrubute in elements vpk{x} VKICKER elements.
#
# This routine transfers the values of the correctors from the
# lattice_with_set_correctors to lattice_to_fix.
#
# I'm relying heavily on python's using references to objects in containers.
# I hope it works.
def set_painting_kickers(lattice_to_fix, lattice_with_set_correctors):
    hcorrectors = { "hs09": None, "hs10": None, "hs11": None, "hs12": None}
    vcorrectors = { "vpk1": None, "vpk2": None, "vpk3": None, "vpk4": None,
                    "orbumpa_1": None, "orbumpa_2": None, "orbumpa_3": None,
                    "orbumpa_4": None}

    # read the lattice_with_set_correctors to get values
    for elem in lattice_with_set_correctors.get_elements():
        nm = elem.get_name()
        if nm in hcorrectors:
            if elem.get_type() == "hkicker":
                # the booster lattice uses the incorrect attribute hkick
                # for these hkicker elements.
                hcorrectors[nm] =  elem.get_double_attribute("hkick")
            else:
                raise RuntimeError('lattice_with_set_correctors element {} has incorrect type {}, should be hkicker'.format(nm, elem.get_type()))

        elif nm in vcorrectors:
            if elem.get_type() == "vkicker":
                vcorrectors[nm] = elem.get_double_attribute("kick")
            else:
                raise RuntimeError('lattice_with_set_correctors element {} has incorrect type {}, should be vkicker'.format(nm, elem.get_type()))

    #  end loop over lattice_with_set_correctors

    # All the correctors should have values now
    if None in hcorrectors.values():
        raise RuntimeError('not all hcorrectors values set')
    if None in vcorrectors.values():
        raise RuntimeError('not all vcorrectors values set')

    # Now we have to set them in the lattice_to_fix!
    for elem in lattice_to_fix.get_elements():
        nm = elem.get_name()
        if nm in hcorrectors:
            if elem.get_type() == "hkicker":
                # the booster lattice uses the incorrect attribute hkick
                # for these hkicker elements.
                elem.set_double_attribute('hkick', hcorrectors[nm])
            else:
                raise RuntimeError('lattice_to_fix element {} has incorrect type {}, should be hkicker'.format(nm, elem.get_type()))

        elif nm in vcorrectors:
            if elem.get_type() == "vkicker":
                elem.set_double_attribute('kick', vcorrectors[nm])
            else:
                raise RuntimeError('lattice_to_fix element {} has incorrect type {}, should be vkicker'.format(nm, elem.get_type()))

